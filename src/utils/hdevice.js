/**hdevice */
import IosParser from './ios.js';
import AndroidParser from './android.js';
class Hdevice {
  constructor() {
    console.log("constructor");
  }
  init() {
    this.ua = navigator.userAgent;
    if (this.ua) {
      this.ua = this.ua.toLocaleLowerCase();
    }
  }
  /**
         * 订阅事件
         * @param {*} type 
         * @param {*} callBack 
         * @param {*} flag 
         */
  on(type, callBack, flag) {
    //创建_events对象
    if (!this._events) {
      this._events = Object.create(null);
    }
    // 判断_events对象是否有type属性
    if (this._events[type]) {
      if (flag) {
        this._events[type].unshift(callBack)
      } else {
        this._events[type].push(callBack)
      }

    } else {
      this._events[type] = [callBack]
    }
    // 超出最大绑定事件限制提示
    if (this._events[type].length >= 10) {
      console.log("超出最大绑定事件限制")
    }
  }
  /**
   * 发布事件
   * @param {*} type 
   * @param  {...any} args 
   */
  emit(type, ...args) {
    if (this._events[type]) {
      this._events[type].forEach(fn => {
        fn.call(this, ...args)
      });
    }
  }
  static getQueryString() {
    var vars = [],
      hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars[name];
  }

  static debug() {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = "https://cdn.bootcss.com/vConsole/3.2.0/vconsole.min.js";
    script.onload = script.onreadystatechange = function () {
      if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
        new VConsole();
      }
    };
    head.appendChild(script);
  }
  static getResult() {
    this.getQueryString("mode") === "debug" ? this.debug() : '';
    console.log(navigator.userAgent);
    let obj = {}
    obj.system = this.detectOS();
    this.system = obj.system;
    obj.system_version = this.getOSVersion();
    obj.browser = this.getExplore()["browser"];
    obj.browser_version = this.getExplore()["browser_version"]
    obj.is_weixn = this.is_weixn();
    obj.device_type = this.getDeviceType();
    try {
      obj.deviceMemory = navigator.deviceMemory;
    } catch (errors) {
      console.log("获取设备内存失败");
    }
    return obj;
  }
  static getResultAsync() {
    return new Promise((resolve, rejects) => {
      try {
        resolve(this.getResult());
      } catch (error) {
        rejects(error);
      }
    })
  }
  /**
   * 获取系统平台
   */
  static detectOS() {
    let sUserAgent = navigator.userAgent;
    let isWin = (navigator.platform == "Win32") || (navigator.platform == "Windows");
    let isMac = (navigator.platform == "Mac68K") || (navigator.platform == "MacPPC") || (navigator.platform == "Macintosh") || (navigator.platform == "MacIntel");
    if (isMac) return "mac";
    // let isUnix = (navigator.platform == "X11") && !isWin && !isMac;
    // if (isUnix) return "unix";
    let isLinux = (String(navigator.platform).indexOf("Linux") > -1);
    if (isLinux) {
      let isAndroid = sUserAgent.indexOf('Android') > -1 || sUserAgent.indexOf('Adr') > -1;
      if (isAndroid) {
        return "android";
      }
      let isiOS = !!sUserAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
      if (isiOS) {
        return "ios";
      }
      return "linux";
    }
    if (isWin) {
      let isAndroid = sUserAgent.indexOf('Android') > -1 || sUserAgent.indexOf('Adr') > -1;
      if (isAndroid) {
        return "android";
      }
      let isiOS = !!sUserAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
      if (isiOS) {
        return "ios";
      }
      return "window";
    }
    let isAndroid = sUserAgent.indexOf('Android') > -1 || sUserAgent.indexOf('Adr') > -1;
    if (isAndroid) {
      return "android";
    }
    let isiOS = !!sUserAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
    if (isiOS) {
      return "ios";
    }
    return "other";
  }
  /**
   * 获取系统版本
   */
  static getOSVersion() {
    let ua = navigator.userAgent.toLowerCase();
    if (this.detectOS() === "android") {
      return "android" + ua.match(/android (\S*);/) ? ua.match(/android (\S*);/)[1] : '';
    }
    if (this.detectOS() === "ios") {
      return "android" + ua.match(/os (\S*) like/) ? ua.match(/os (\S*) like/)[1] : '';
    }
    if (this.detectOS() === "window") {
      let sUserAgent = navigator.userAgent;
      let isWin2K = sUserAgent.indexOf("Windows NT 5.0") > -1 || sUserAgent.indexOf("Windows 2000") > -1;
      if (isWin2K) return "win2000";
      let isWinXP = sUserAgent.indexOf("Windows NT 5.1") > -1 || sUserAgent.indexOf("Windows XP") > -1;
      if (isWinXP) return "winxp";
      let isWin2003 = sUserAgent.indexOf("Windows NT 5.2") > -1 || sUserAgent.indexOf("Windows 2003") > -1;
      if (isWin2003) return "win2003";
      let isWinVista = sUserAgent.indexOf("Windows NT 6.0") > -1 || sUserAgent.indexOf("Windows Vista") > -1;
      if (isWinVista) return "winvista";
      let isWin7 = sUserAgent.indexOf("Windows NT 6.1") > -1 || sUserAgent.indexOf("Windows 7") > -1;
      if (isWin7) return "win7";
      let isWin8 = sUserAgent.indexOf("Windows NT 6.2") > -1 || sUserAgent.indexOf("Windows 8") > -1;
      if (isWin8) { return "win8"; }
      let isWin10 = sUserAgent.indexOf("Windows NT 10.0") > -1 || sUserAgent.indexOf("Windows 10") > -1;
      if (isWin10) { return "win10"; }
    }
  }
  /**
   * 获取设备型号
   */
  static getDeviceType() {
    let ua = navigator.userAgent.toLowerCase();
    if (this.system === "ios") {
      let iosParser = new IosParser(ua);
      return iosParser.getModels();
    }
    if (ua.match(/ipad/i) == "ipad") { return "ipad" }
    if (this.system === "android") {
      let andriodParser = new AndroidParser(ua);
      return andriodParser.getDeviceType();
    }
  }
  static is_weixn() {
    let ua = navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == "micromessenger") {
      return true;
    } else {
      return false;
    }
  }
  static getExplore() {
    let Sys = {};
    let ua = navigator.userAgent.toLowerCase();
    let s;
    (s = ua.match(/rv:([\d.]+)\) like gecko/)) ? Sys.ie = s[1] :
      (s = ua.match(/msie ([\d\.]+)/)) ? Sys.ie = s[1] :
        (s = ua.match(/ucbrowser\/([\d\.]+)/)) ? Sys.ucbrowser = s[1] :
          (s = ua.match(/edge\/([\d\.]+)/)) ? Sys.edge = s[1] :
            (s = ua.match(/firefox\/([\d\.]+)/)) ? Sys.firefox = s[1] :
              (s = ua.match(/(?:opera|opr).([\d\.]+)/)) ? Sys.opera = s[1] :
                (s = ua.match(/chrome\/([\d\.]+)/)) ? Sys.chrome = s[1] :
                  (s = ua.match(/version\/([\d\.]+).*safari/)) ? Sys.safari = s[1] : 0;
    // 根据关系进行判断
    if (Sys.ie) return { browser: "ie", browser_version: Sys.ie };
    if (Sys.edge) return { browser: "edge", browser_version: Sys.edge };
    if (Sys.firefox) return { browser: "firefox", browser_version: Sys.firefox };
    if (Sys.chrome) return { browser: "chrome", browser_version: Sys.chrome };
    if (Sys.opera) return { browser: "opera", browser_version: Sys.opera };
    if (Sys.safari) return { browser: "safari", browser_version: Sys.safari };
    if (Sys.ucbrowser) return { browser: "uc", browser_version: Sys.ucbrowser };
    this.feedback();// 反馈
    return { browser: "", browser_version: "" };
  }
  /**
   * feedback
   */
  feedback(){
    /**
     * 预留
     */
  }
}
window.hdevice = Hdevice;
export default Hdevice;